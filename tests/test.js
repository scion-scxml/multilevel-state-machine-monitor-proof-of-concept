const fs = require('fs')
  tmp = require('tmp'),
  scxml = require('@scion-scxml/scxml'),
  core = require('@scion-scxml/core'),
  path = require('path'),
  afterTick2ExpectedAllSerializedSessions = require('./serialized-interpreter-states/after-tick2.json');

const {
  deserializeAllSerializedSessions,
  initializeRootSessionToSerializeAutomaticallyOnBigStepEndAndInvokedSessionInitialized
} = require('..')

const executionContext = {
  console: console,
};

let fnModel, rootSession;

const docUrlRe = /tests(\/|\\)scxml(\/|\\).*\.scxml$/
function allSessionsAbsPathsToRelativePaths(allSerializedSessions){
  const allSerializedSessions2 = {}
  Object.keys(allSerializedSessions).forEach( key => {
    const o = allSerializedSessions[key]
    allSerializedSessions2[key] = {
      ...o,
      docUrl : o.docUrl.match(docUrlRe)[0].replace(/\\/g,'/') //convert docUrl to relative path
    }
  })
  return allSerializedSessions2 
}

module.exports = {
  setUp: function (callback) {
    scxml.pathToModel(path.join(__dirname,'scxml/main.scxml'), function(err,model){
      if(err) throw err;
      model.prepare((err, _fnModel) => {
        if(err) throw err;
        fnModel = _fnModel

        //instantiate the interpreter
        rootSession = new core.Statechart(fnModel);

        //start the machine
        rootSession.start();    

        callback();
      }, executionContext)
    })
  },
  tearDown: function (callback) {
    // TODO: clean up
    callback();
  },
  test1: function (test) {
    let allSerializedSessions = initializeRootSessionToSerializeAutomaticallyOnBigStepEndAndInvokedSessionInitialized(rootSession)
    rootSession.on('onInvokedSessionInitialized', invokedInterpreter => {
      const invokedFileName = path.basename(invokedInterpreter._model.docUrl,'.scxml')
      switch(invokedFileName){
        case 'sub1':
          invokedInterpreter.on('onBigStepEnd',() => {
            if(invokedInterpreter.getConfiguration()[0] === 'start'){
              // wait until sub1 has entered start state, then send first tick event
              invokedInterpreter.gen({ name: "tick", data: 1 });
            }
          })
          break
        case 'sub2':
          invokedInterpreter.on('onBigStepEnd',() => {
            if(invokedInterpreter.getConfiguration()[0] === 'start'){
              // wait until sub2 has entered start state, then send second tick event
              invokedInterpreter.gen({ name: "tick", data: 2 });

              //assert the state
              //test.equal(allSerializedSessions, afterTick2ExpectedAllSerializedSessions)
              test.deepEqual(allSessionsAbsPathsToRelativePaths(allSerializedSessions), allSessionsAbsPathsToRelativePaths(afterTick2ExpectedAllSerializedSessions))
              // dump state to filsystem for good measure
              const tmpobj = tmp.fileSync()
              console.log('Sessions tempfile: ', tmpobj.name);
              fs.writeFileSync(tmpobj.fd, JSON.stringify(allSerializedSessions,null, 4))
              const allSerializedSessions2 = JSON.parse(fs.readFileSync(tmpobj.name))

              //deserialize the state loaded from the filesystem
              debugger
              deserializeAllSerializedSessions(allSerializedSessions2, (err, deserializedRootSession) => {
                if(err) throw err;

                //re-register the event listener
                deserializedRootSession.on('onInvokedSessionInitialized', invokedInterpreter => {
                  const invokedFileName = path.basename(invokedInterpreter._model.docUrl,'.scxml')
                  switch(invokedFileName){
                    case 'sub3':
                      invokedInterpreter.on('onBigStepEnd',() => {
                        if(invokedInterpreter.getConfiguration()[0] === 'start'){
                          deserializedRootSession.on('onBigStepEnd',() => {
                            const config = deserializedRootSession.getConfiguration()
                            test.deepEqual(config, ['end'])
                            test.done();
                          })

                          // wait until sub3 has entered start state, then send third tick event
                          invokedInterpreter.gen({ name: "tick", data: 4 });
                        }
                      })
                      break
                  }
                })

                deserializedRootSession._scriptingContext._invokeMap['sub2'].then( sub2Session => {
                  // get the sub-session and send it tick3 event
                  sub2Session.gen({ name: "tick", data: 3 });
                  debugger
                })
              })
            }
          })
          break
      }
    })
  }
};
