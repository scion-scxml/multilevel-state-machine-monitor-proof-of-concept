const fs = require('fs')
const core = require('@scion-scxml/core')
const util = require('util')

const stateFile = 'serialized-interpreter-states.json'
function persistInterpretersToFilesystem(allSerializedSessions){
  fs.writeFileSync(stateFile, JSON.stringify(allSerializedSessions,null, 4))
}

function serializeInvokeMap(invokeMap){
  const invokeIds = Object.keys(invokeMap)
  return Promise.all(
    invokeIds.map( invokeId => 
      invokeMap[invokeId]
    )
  ).then(invokedSessions => {
    const o = {}
    invokedSessions.forEach( (session, index) => o[invokeIds[index]] = session.opts.sessionid )
    return o
  })
}

function handleInterpreterBigStepEnd(interpreter, allSerializedSessions){
  interpreter.on('onBigStepEnd',(e) => {
    if((e && e.name === "done.state.$generated-scxml-0") || interpreter.isFinal()) return;
    // persist the state machine state
    const sessionid = interpreter.opts.sessionid,
      invokeid = interpreter.opts.invokeid,
      snapshot = interpreter.getSnapshot(),
      docUrl = interpreter._model.docUrl;
    console.log(sessionid, invokeid, interpreter.opts._invokeMap)
    //console.log('persist state machine snapshot', snapshot)
    if(allSerializedSessions[sessionid]){
      allSerializedSessions[sessionid].snapshot = snapshot
    }else{
      allSerializedSessions[sessionid] = {
        sessionid,
        invokeid,
        snapshot,
        docUrl,
        invokeMap : null  //TODO: invokeMap will be updated in onInvokedSessionInitialized
      }
    }
    //persistInterpretersToFilesystem(allSerializedSessions)
  })
}

function handleInvokedSessionInitialized(rootSession, allSerializedSessions, invokedInterpreter){

  //clear the serialized session on exit
  invokedInterpreter.on('onExitInterpreter', () => {
    const sessionid = invokedInterpreter.opts.sessionid;

    if(allSerializedSessions[sessionid]){
      delete allSerializedSessions[sessionid]
    }
    //persistInterpretersToFilesystem(allSerializedSessions)
  });

  process.nextTick(() => {
    serializeInvokeMap(rootSession._scriptingContext._invokeMap).then(serializedInvokeMap => {

      const sessionid = rootSession.opts.sessionid,
        invokeid = rootSession.opts.invokeid,
        snapshot = rootSession.getSnapshot(),
        docUrl = rootSession._model.docUrl;

      if(allSerializedSessions[sessionid]){
        allSerializedSessions[rootSession.opts.sessionid].invokeMap = serializedInvokeMap 
      }else{
        allSerializedSessions[sessionid] = {
          sessionid,
          invokeid,
          snapshot,
          docUrl,
          invokeMap : serializedInvokeMap
        }
        if(allSerializedSessions[sessionid].invokeid === undefined){
          delete allSerializedSessions[sessionid].invokeid
        }
      }
      //persistInterpretersToFilesystem(allSerializedSessions)
    })
  })

  handleInterpreterBigStepEnd(invokedInterpreter, allSerializedSessions)
}


// this is recursive
// https://en.wikipedia.org/wiki/Tree_traversal#Post-order,_LRN
function postorderTraversal(parentSession, allSerializedSessions, invokeMap, serializedSession, callback){
  
  // 1. Traverse the subtree by recursively calling the post-order function.
  // 2. Access the data part of the current node (in the figure: position blue).
  const serializedInvokeMap = serializedSession.invokeMap

  // get the fnModel
  

  scxml.pathToModel(serializedSession.docUrl, function(err,model){
    if(err) return callback(err);

    const executionContext = {
      console: console,
    };
    model.prepare((err, fnModel) => {
      if(err) return callback(err);

      //instantiate the interpreter
      // TODO: connect parentSession
      const session = new core.Statechart(fnModel, {
        sessionid : serializedSession.sessionid,
        invokeid : serializedSession.invokeid,
        snapshot: serializedSession.snapshot,
        parentSession
      })

      //handleInterpreterBigStepEnd(session)
      session._scriptingContext._invokeMap = invokeMap

      if(!serializedInvokeMap){
        return callback(null, session)
      } 

      Promise.all(
        Object.keys(serializedInvokeMap).map( invokeId => {
          const sessionid = serializedInvokeMap[invokeId]
          return util.promisify(postorderTraversal)(session, allSerializedSessions, invokeMap, allSerializedSessions[sessionid])
        })
      ).then( childSessions => {
        childSessions.forEach(childSession => {
          invokeMap[childSession.opts.invokeid] = Promise.resolve(childSession)
        })

        return callback(null, session)
      })
    }, executionContext)
  })
}

function deserializeAllSerializedSessions(allSerializedSessions, callback) {
  const rootSerializedInterpreterSessionId = Object.keys(allSerializedSessions).find( sessionId => !allSerializedSessions[sessionId].invokeid ) 

  const rootSerializedInterpreter = allSerializedSessions[rootSerializedInterpreterSessionId]

  const invokeMap = {}
  postorderTraversal(null, allSerializedSessions, invokeMap, rootSerializedInterpreter, callback)
}

function initializeRootSessionToSerializeAutomaticallyOnBigStepEndAndInvokedSessionInitialized(rootSession){
  let allSerializedSessions = {}
  handleInterpreterBigStepEnd(rootSession, allSerializedSessions);
  rootSession.on('onInvokedSessionInitialized', handleInvokedSessionInitialized.bind(this, rootSession, allSerializedSessions));
  return allSerializedSessions 
}

module.exports = {
  deserializeAllSerializedSessions,
  initializeRootSessionToSerializeAutomaticallyOnBigStepEndAndInvokedSessionInitialized
}
